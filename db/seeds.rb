# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Status.delete_all
User.delete_all
Status.create(
   android_users: 3700,
   iphone_users: 3700,
   active_users: 5076,
   active_qicard: 3139,
   new_users_this_week: 783,
   total_balance_enquires: 192424,
   total_balance_enquires_this_week: 24579,
   zain_numbers: 2388,
   asiacell_numbers: 2466
)

User.create(
   name: "Nameer",
   email: "nameer6g6@gmail.com",
   password: "nameer",
   password_confirmation: "nameer"
)

