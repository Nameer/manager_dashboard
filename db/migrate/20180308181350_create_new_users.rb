class CreateNewUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :new_users do |t|
      t.integer :new_users_number

      t.timestamps
    end
  end
end
