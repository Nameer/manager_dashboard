class CreateAndroidUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :android_users do |t|
      t.integer :android_users_number

      t.timestamps
    end
  end
end
