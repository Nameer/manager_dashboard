class CreateActiveUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :active_users do |t|
      t.integer :user_numbers

      t.timestamps
    end
  end
end
