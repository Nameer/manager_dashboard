class CreateIphoneUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :iphone_users do |t|
      t.integer :iphone_users_number

      t.timestamps
    end
  end
end
