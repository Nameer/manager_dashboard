class CreateActiveQicards < ActiveRecord::Migration[5.1]
  def change
    create_table :active_qicards do |t|
      t.integer :active_qicard_number

      t.timestamps
    end
  end
end
