class CreateStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :statuses do |t|
      t.integer :android_users
      t.integer :iphone_users
      t.integer :active_users
      t.integer :active_qicard
      t.integer :new_users_this_week
      t.integer :total_balance_enquires
      t.integer :total_balance_enquires_this_week
      t.integer :zain_numbers
      t.integer :asiacell_numbers

      t.timestamps
    end
  end
end
