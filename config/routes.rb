Rails.application.routes.draw do

   resources :dashboard, expect: [:edit, :new, :create, :show, :destroy] do
      collection do
	 get :prototype
      end
   end

   devise_scope :user do
      get "/sign_in"	      =>    "devise#session_new"
      get "/users/sign_out"    =>    "devise#session_destroy"
   end

   devise_for :users, skip: :registration
   # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

   authenticated :user do
      root 'dashboard#index'
   end

   unauthenticated :user do
      root 'public#main_page'
   end

   namespace :charts do
      get "active_users"
      get "phones"
      get "sim_card_type"
      get 'enquires'
   end

   namespace :tables do
      get 'zain_users'
   end
end
