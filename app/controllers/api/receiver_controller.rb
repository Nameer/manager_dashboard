class Api::ReceiverController < ApplicationController

   def update
      data =  Status.find(params[:id])
      if data.update(status_params)
	 render status: 200, json: { message: "updated successfully" }
      else
	 render status: 422, json: { message: "Did not updated" }
      end
   end

   def create
   end

   private 

   def status_params
      params.require(:status).permit(:android_users, :iphone_users, :active_qicard, :active_users, :new_users_this_week, :total_balance_enquires, :total_balance_enquires_this_week, :zain_numbers, :asiacell_numbers)
   end
end
