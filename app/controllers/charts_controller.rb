class ChartsController < ApplicationController
   
   def phones
      #render json: Status.group(:iphone_users, :android_users).last
      android = Status.pluck(:android_users).last
      iphone = Status.pluck(:iphone_users).last
      render json: { "IPhone Users" => iphone, "Android Users" => android }
   end
   
   def sim_card_type
      zain = Status.pluck(:zain_numbers).last
      asia = Status.pluck(:asiacell_numbers).last
      render json: {"Zain Users" => zain, "Asiacell Users" => asia }
   end


   def active_users
      active	  = Status.pluck(:active_users).last
      this_week	  = Status.pluck(:new_users_this_week).last 
      render json: [["Total Active Users", active],["Last Week New Users",this_week]]
   end

   def enquires
      total	  = Status.pluck(:total_balance_enquires).last
      this_week	  = Status.pluck(:total_balance_enquires_this_week).last 
      render json: [["Total Enquires", total],["Last week enquires",this_week]]
   end


end
