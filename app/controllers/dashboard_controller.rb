class DashboardController < ApplicationController

   before_action :authenticate_user!

   layout 'prototype', only: :prototype

   def index
      @active_users = Status.pluck(:active_users).last
   end

   def prototype
      @active_users = Status.pluck(:active_users).last
   end
end
